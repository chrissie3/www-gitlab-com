---
title: "Why it's crucial to break things down into smallest iterations"
author: Matej Latin
author_gitlab: matejlatin
author_twitter: matejlatin
categories: unfiltered
image_title: '/images/blogimages/smallest-iterations/cover.jpg'
description: "How we learned from our mistakes and adapted our mentality to always find the smallest thing we can do to learn and reduce the risk of shipping something that doesn't add value."
tags: growth, inside GitLab, UX
---

{::options parse_block_html="true" /}

<i class="fab fa-gitlab" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>&nbsp;&nbsp;
This blog post is [Unfiltered](/handbook/marketing/blog/unfiltered/#legal-disclaimer)
&nbsp;&nbsp;<i class="fab fa-gitlab" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>
{: .alert .alert-webcast}

I recently wrote a blog post called [Small experiments, significant results](https://11367-matej-smallest-iterations-post.about.gitlab-review.app/blog/2021/04/07/small-experiments-significant-results-and-learnings/) about our recent successes in conducting small experiments. But that wasn't the whole story. I didn't tell you about the early failures that we learned from to get to these small experiments. This blog post tells that part of the story.

When the Growth team was formed at GitLab in late 2019, we had little experience with designing, implementing and shipping experiments. We hired experienced people but it was still hard to predict how long it would take to implement and ship an experiment. The *Suggest a pipeline* experiment was the first one I worked on with the Growth:Expansion team. The idea was simple: guide users through our UI to help them set up a CI/CD pipeline.

![The guided tour entry](/images/blogimages/smallest-iterations/suggest.png)

[The original prototype of the guided tour](https://www.sketch.com/s/1794d37d-c722-4d32-862e-9c6c5d831149/a/zn1Z9o/play)

The guided tour would start on the merge request page and ask the user if they want to learn how to set up a CI/CD pipeline. Those who opted in would be guided through the three steps required to complete the setup. The team saw this as a simple 3-step guide, so we committed ourselves to ship it without considering if it was the smallest thing to do. We wanted to create a guided tour because it hadn't been done yet at GitLab. But it turned out that this wasn't the right way to think about this. Our thinking now is: "what's the smallest thing we can test and learn from?"

One of GitLab's company values is [Iteration](https://about.gitlab.com/handbook/values/#iteration) which means that we strive to do *the smallest thing possible and get it out as quickly as possible*. The concept of MVC (minimal viable change) helps us with that and is described in our Handbook as:

> We encourage MVCs to be as small as possible. Always look to make the quickest change possible to improve the user's outcome.

Looking back, I now realize we failed to do that with the *Suggest a pipeline* experiment. But I'm grateful for that mistake because it provided us with one of the most valuable lessons: *always try the smallest viable thing first!* Even, or maybe especially, with experiments.

Here's a quick overview of why it's important to break things down into small iterations for:

- It gets value to the user faster.
- It decreases the risk of shipping something that doesn't add value.
- It is easier to isolate and understand the impact of the changes.
- It is faster to ship, so the team starts learning sooner.
- The team can start thinking about further iterations sooner or decide to abandon the experiment earlier (before too much time and resources are wasted).

![Small vs large iterations](/images/blogimages/smallest-iterations/chart.jpg)

In the figure above, Team 1 in the non-experimental work shipped a smaller iteration quickly and iterated on it twice, while Team 2 only shipped one large iteration in the same time. Team 1 learned from their first small iteration and adapted their solution twice. Team 2 decided to ship a larger iteration and so it took them longer and they sacrificed learnings they could have used to optimize their solution.

In the experimental work, Team 1 shipped a smaller first iteration, reviewed results and could then decide whether to iterate further or abandon idea 1 and move on to idea 2. By doing so, they could either ship three iterations of idea 1 or abandon it and start working on idea 2 and ship the first iteration for it. All that in the same amount of time as it took Team 2 to ship a larger first iteration of idea 1. Team 1 is much more likely to come to successful results and learnings.

## How the *Suggest a pipeline* experiment should have been done

It's easy to look back now and see what we did wrong but such reflection allows us to avoid repeating mistakes. The guided tour looked like a simple thing to build and ship as an experiment but in the end it wasn't. It took months to complete. The experiment was successful overall. After we took another look at it months later (when implementation was completed), it was clear that it could be improved. Despite the experiment being successful, we decided to iterate on the copy in the first nudge, trying to get even more users to opt in. If we shipped a smaller experiment sooner, we could have started iterating sooner and we would have ended up with the optimal version of the first nudge sooner, so more users could benefit from the guide sooner.

![Copy changes](/images/blogimages/smallest-iterations/copy-changes.jpg)

Because it took us months to complete the implementation of the experiment, it also took us months to iterate on it.

If I had to do a similar experiment now, I'd start much smaller, with something that could be built and shipped in less than a month, ideally even faster. For example, we could have shipped an iteration with that first nudge linking to an existing source that explains how to set up a pipeline. That would have enabled us to validate the placement of the nudge, its content, and its effectiveness. It would have significantly reduced the risk of the experiment.

Or maybe we could have shortened the guided tour to two steps only, which is exactly [what my colleague Kevin did here](https://gitlab.com/gitlab-org/growth/product/-/issues/1662/). But because our idea already seemed like a small iteration, we never felt the urgency to reduce it further. So here's another reason why it's important to really think about the smallest possible iteration first: you can never be sure that what you're aiming to do will actually be as quick and simple as expected. So even when you think that your idea is the smallest possible iteration, *think again*.

## Applying these learnings to the *Invite members* experiment

When I started working on it, the vision of what the *Invite members* experience should be like was way more complex than the *Suggest a pipeline* guided tour. The idea was that any user could invite their team members to a project and an admin user would have to approve the invitation. But because of our learnings from the pipeline experiment, we decided to simplify the first experiment. Instead of designing and building a whole experience, we decided to use a [painted door test](https://crstanier.medium.com/a-product-managers-guide-to-painted-door-tests-a1a5de33b473). This involved displaying an invite link that on click, displayed a message to users that the feature wasn't ready and suggested a temporary solution. This allowed us to validate the riskiest part of the experiment: do non-admin users even want to invite their colleagues?

![Not ready modal](/images/blogimages/smallest-iterations/modal-not-ready.png)

## Conclusion

We were lucky with the *Suggest a pipeline* experiment. It was the first experiment we worked on, and it was “low hanging fruit", so the chance of failing was less likely. But as we move away from obvious improvements and start exploring more risky experiments, we won't be able to rely on luck. We need to be diligent and break things down into small experiments or iterations to reduce the risk of spending time on things that don't add value to our users and don't have a positive impact on our company’s growth.

Photo by [Markus Spiske](https://unsplash.com/@markusspiske?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/s/photos/pieces?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
{: .note}
