---
layout: handbook-page-toc
title: "OSS Contributions"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Contributions to OSS

We believe in Open Source and want to lead by example, contributing to GitLab and the OSS landscape.

### Definition of Contributions

Contributions are "more than just code" and are often times hard to measure. We try to start with a small subset and update this section over time.

- Source Code
- Documentation
- Tutorials and blog posts
- Workshops and trainings
- Issue discussion, resolving bugs and suggesting feature design/architecture
- Package and release distribution
- Help on community forums and social media

### Resource and Time Dedication


Developer Evangelists are encouraged to find time every week for Open Source contributions. 

### Measurement

We aim to measure this in the form of submitted issues, MR/PRs and commit URLs where applicable. 

### Contribution Areas

It can be hard to decide, or make a one-time contribution a permanent engagement in a project, leading the way to maybe help maintain it in the future. As a first iteration, we define the requirements for qualifying projects:

- Use them on a daily basis (e.g. CLI tools)
- Help you and your environment (e.g. a calendar app)
- Benefits the ecosystem and makes processes easier

We focus on languages and frameworks where we have the most experience: Golang, C/C++, JS, Ruby, .NET, etc. 

### Projects

The following projects have been evaluated for contributions. Each of them lists issues and ideas for future contributions.

#### GitLab

- [GitLab CLI](https://gitlab.com/groups/gitlab-org/-/epics/3324)
- [Make it easier to contribute](https://docs.gitlab.com/ee/development/contributing/)
  - Gitpod integration and showcases 
  - MR review guidance 
- Documentation
  - [CI/CD Pipeline Efficiency](https://docs.gitlab.com/ee/ci/pipelines/pipeline_efficiency.html) sourcing from the [CI Monitoring webcast](https://learn.gitlab.com/c/deep-monitoring-ci?x=fDT7Bl)

#### Gitpod

- Add Gitpod integration to projects
  - [Waypoint Demo](https://gitlab.com/brendan-demo/waypoint/-/blob/main/.gitpod.Dockerfile)
  - [everyonecancontribe.com GitLab pages](https://gitlab.com/everyonecancontribute/everyonecancontribute.gitlab.io/-/merge_requests/61)

#### Prometheus

- [Repository](https://github.com/prometheus/prometheus)
- Documentation
- Packages (blocked by GitLab package registry)
- Node Exporters
  - [GitLab CI Pipeline Exporter](https://github.com/mvisonneau/gitlab-ci-pipelines-exporter)

#### OpenTelemetry

- Help with work on the [collector](https://github.com/open-telemetry/opentelemetry-collector)
- SDKs for specific languages: 
  - [C++](https://github.com/open-telemetry/opentelemetry-cpp)
  - [Golang](https://github.com/open-telemetry/opentelemetry-go) 

### HashiCorp

#### Waypoint

- Documentation
  - [GitLab CI/CD integration PR](https://github.com/hashicorp/waypoint/pull/492)
- Blog posts
  - [How to use HashiCorp Waypoint to deploy with GitLab CI/CD](https://about.gitlab.com/blog/2020/10/15/use-waypoint-to-deploy-with-gitlab-cicd/)
- GitLab Integration
  - [Waypoint Images](https://gitlab.com/gitlab-org/waypoint-images)
  - [CI/CD templates](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/45314)
- Demo
  - [Waypoint AWS ECS example](https://gitlab.com/brendan-demo/waypoint)
- Community
  - [5. everyonecancontribute cafe](https://everyonecancontribute.com/post/2020-10-21-cafe-5-hashicorp-waypoint/)

#### Terraform

- [terraform-provider-gitlab](https://github.com/gitlabhq/terraform-provider-gitlab)

### Puppet

Help maintain the GitLab modules.

- [puppet-gitlab](https://github.com/voxpupuli/puppet-gitlab)
- [puppet-gitlab_ci_runner](https://github.com/voxpupuli/puppet-gitlab_ci_runner)



