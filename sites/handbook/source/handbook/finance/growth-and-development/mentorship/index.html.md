---
layout: handbook-page-toc
title: Finance Mentorship Program
---

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

### Finance Mentorship Program
As discussed in [GitLab’s Remote Playbook](https://about.gitlab.com/company/culture/all-remote/), the intentionality behind communication, especially in remote settings, is a critical element of success. This pilot program aims to support GitLab’s communications plan by purposefully creating and cultivating opportunities for mentors, mentees, and connection.

In the finance team, there's a great untapped opportunity for cross-training and relationship building. We are launching our first mentorship pilot program to establish whether mentorship is a good way for the finance team to faciliate learning and development and normalise knowledge sharing in our department.

#### Program Structure

##### 1:1 Coaching Sessions
The program proposes a mentor/mentee relationship between selected applicants on the sales team and mentors in leadership roles across the company. Sessions will take place every other week for 30-minutes and will last for up to 3 months (with the possibility of an extension), as long as both mentors and mentees remain engaged. The mentor/mentee relationship will be cross-divisional, meaning that both parties will have the opportunity to work with and learn from team members outside of your respective divisions.

##### Program Slack Channel
All program communications will be sent through the slack channel [#finance-mentorship-program](https://gitlab.slack.com/archives/C01SELK5M18).

##### Program Timeline

| Phase                             | Timeline                      |
| -------                           | -------                       |
| Call for Mentors and Mentees      | 5 April - 16 April 2021       |
| Pairing process                   | 19 April - 23 April 2021      |
| Mentor program kickoff meeting    | Monday 26th April 2021        |
| Mentorship period                 | 26 April - 30 July 2021     |
| Mid-program feedback survey       | 16 June 2021                |
| End of program feedback survey    | 2 August 2021             |

#### Engagement Criteria
The program will last for up to 3-months if both the mentor and the mentee remain engaged. Being engaged in the program will be defined as:

* Attending all scheduled sessions
* Actively participating in all sessions
* Preparing for calls (mentees will drive agenda)
* Implementing learnings (namely for mentees)

#### Program Participants

| Mentor | Mentee |
| ------- | ------- |
|  |  |

#### Success Metrics
* 80 or greater NPS score across mentors and mentees (9's or 10's for the following question: "Overall, I would recommend this program to another GitLab team member" at the end of the pilot program survey).
* 100% participation in program-related calls

#### DRIs
[@dparker](http://gitlab.com/dparker) and [@bryanwise](https://gitlab.com/bryanwise)
